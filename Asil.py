#!/usr/bin/env python
# coding: utf-8

# In[1]:


#!/usr/bin/env python -W ignore::DeprecationWarning


# In[2]:

import sys
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.lancaster import LancasterStemmer
import nltk
import re
from sklearn.preprocessing import OneHotEncoder
import matplotlib.pyplot as plt
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.models import Sequential, load_model
from keras.layers import Dense, LSTM, Bidirectional, Embedding, Dropout
from keras.callbacks import ModelCheckpoint
import warnings
from collections import OrderedDict
warnings.filterwarnings('ignore')


# In[3]:


path = sys.argv[1]
print(path);
def load_dataset(filename):
    df = pd.read_csv(filename, encoding = "utf-8", names = ["Sentence", "Intent"])
   # print(df.head())
    intent = df["Intent"]
    unique_intent = list(OrderedDict.fromkeys(intent))
    sentences = list(df["Sentence"])
    return (intent, unique_intent, sentences)


# In[4]:


intent, unique_intent, sentences = load_dataset("/home/benan/Desktop/intentClassifier/newDataset.csv")


# In[5]:


arr=[]
for i in sentences:
    arr.append(i)


# In[6]:


lower_map = {
    ord(u'ı'): u'i',
    ord(u'İ'): u'i',
    ord(u'ö'): u'o',
    ord(u'Ö'): u'o',
    ord(u'ü'): u'u',
    ord(u'ü'): u'u',
    ord(u'Ç'): u'c',
    ord(u'ç'): u'c',
    ord(u'Ğ'): u'g',
    ord(u'ğ'): u'g',
    ord(u'ş'): u's',
    ord(u'Ş'): u's'
    }

"""myCity = u'ısık'
lowerCity = myCity.translate(lower_map)
print(lowerCity)"""


# In[7]:


new=[]
for i in arr:
    new_sentence = i.translate(lower_map)
    new.append(new_sentence)


# In[8]:


sentences = new


# In[9]:


nltk.download("stopwords")
nltk.download("punkt")


# In[10]:


#define stemmer
stemmer = LancasterStemmer()


# In[11]:


def cleaning(sentences):
    words = []
    for s in sentences:
        clean = re.sub(r'[^ a-z A-Z 0-9]', " ", s)
        w = word_tokenize(clean)
        #stemming
        words.append([i.lower() for i in w])

    return words


# In[12]:


cleaned_words = cleaning(sentences)


# In[13]:


def create_tokenizer(words, filters = '!"#$%&()*+,-./:;<=>?@[\]^_`{|}~'):
    token = Tokenizer(filters = filters)
    token.fit_on_texts(words)
    return token


# In[14]:


def max_length(words):
      return(len(max(words, key = len)))


# In[15]:


word_tokenizer = create_tokenizer(cleaned_words)
vocab_size = len(word_tokenizer.word_index) + 1
max_length = max_length(cleaned_words)

print("Vocab Size = %d and Maximum length = %d" % (vocab_size, max_length))


# In[16]:


def encoding_doc(token, words):
    return(token.texts_to_sequences(words))


# In[17]:


encoded_doc = encoding_doc(word_tokenizer, cleaned_words)


# In[18]:


def padding_doc(encoded_doc, max_length):
    return(pad_sequences(encoded_doc, maxlen = max_length, padding = "post"))


# In[19]:


padded_doc = padding_doc(encoded_doc, max_length)


# In[20]:


#print("Shape of padded docs = ",padded_doc.shape)


# In[21]:


#tokenizer with filter changed
output_tokenizer = create_tokenizer(unique_intent, filters = '!"#$%&()*+,-/:;<=>?@[\]^`{|}~')


# In[22]:


output_tokenizer.word_index


# In[23]:


encoded_output = encoding_doc(output_tokenizer, intent)


# In[24]:


encoded_output = np.array(encoded_output).reshape(len(encoded_output), 1)


# In[25]:


#encoded_output.shape


# In[26]:


def one_hot(encode):
    o = OneHotEncoder(sparse = False)
    return(o.fit_transform(encode))


# In[27]:


output_one_hot = one_hot(encoded_output)


# In[28]:


# output_one_hot.shape


# In[29]:


model = load_model('/home/benan/Desktop/intentClassifier/model.h5')


# In[30]:


def predictions(text):
    clean = re.sub(r'[^ a-z A-Z 0-9]', " ", text)
    test_word = word_tokenize(clean)
    test_word = [w.lower() for w in test_word]
    test_ls = word_tokenizer.texts_to_sequences(test_word)
    #print(test_word)
    #Check for unknown words
    if [] in test_ls:
        test_ls = list(filter(None, test_ls))
    
    test_ls = np.array(test_ls).reshape(1, len(test_ls))

    x = padding_doc(test_ls, max_length)
    pred = model.predict_proba(x)
  
    return pred


# In[31]:


def get_final_output(pred, classes):
    predictions = pred[0]
    
    classes = np.array(classes)
    ids = np.argsort(-predictions)
    classes = classes[ids]
    predictions = -np.sort(-predictions)
    for i in range(pred.shape[1]):
        if(predictions[i] == (max(predictions))):
            print(classes[i])
	    print(predictions[i])


# In[32]:


text = path
text = text.translate(lower_map)
pred = predictions(text)
#print(unique_intent)
get_final_output(pred, unique_intent)


# In[ ]:





# In[ ]:




